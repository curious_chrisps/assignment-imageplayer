//
//  ImagePlayerGui.hpp
//  ImagePlayer - App
//
//  Created by Christoph Schick on 04/01/2018.
//

#ifndef ImagePlayerGui_hpp
#define ImagePlayerGui_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "../audio/ImagePlayer.hpp"
/**
 GUI class that holds all controls and components related to the ImagePlayer class.
 
 @see ImagePlayer
 */
class ImagePlayerGui    :   public Component,
                            public Button::Listener,
                            public ComboBox::Listener,
                            public FilenameComponentListener,
                            public Slider::Listener,
                            public Timer
{
public:
    /**
     Constructor
     */
    ImagePlayerGui(Audio& audio_, ImagePlayer& imgPlayer_);
    
    /**
     Destructor
     */
    ~ImagePlayerGui();
    
    //Component
    void resized() override;
    
    //Button Listener
    void buttonClicked (Button* button) override ;
    
    // ComboBox Listener
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    //FilenameComponentListener
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged) override;
    
    // Slider Listener
    void sliderValueChanged (Slider* slider) override;
    
    // Gets called periodically to update the slider position.
    void timerCallback() override;
    
private:
    /** References used to initialise the instance of this class with. */
    Audio& audio;
    ImagePlayer& imagePlayer;
    
    /** Button that holds the image preview. */
    ImageButton imgPreview;
    
    /** GUI controls and visual feedback. */
    ScopedPointer<FilenameComponent> fileChooser;
    ComboBox waveChooserBox;
    ComboBox melodyColourChooserBox;
    ComboBox sortingChooserBox;
    
    TextButton playButton;
    TextButton stopButton;
    Slider tempoSlider;
    Slider progressBar;
    Slider volumeSlider;
    
    Label waveLabel;
    Label melodyLabel;
    Label sortingLabel;
    Label volumeLabel;
    Label tempoLabel;
    
};

#endif /* ImagePlayerGui_hpp */

