//
//  ImagePlayerGui.cpp
//  ImagePlayer - App
//
//  Created by Christoph Schick on 04/01/2018.
//

#include "ImagePlayerGui.hpp"

ImagePlayerGui::ImagePlayerGui(Audio& audio_, ImagePlayer& imgPlayer_)
                              : audio (audio_),
                                imagePlayer (imgPlayer_)
{
    fileChooser = new FilenameComponent ("imagefile",
                                         File::nonexistent,
                                         true, false, false,
                                         "*.jpg; *.jpeg; *.png",
                                         String::empty,
                                         "Select an image...");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
    addAndMakeVisible(imgPreview);
    
    waveLabel.setColour(Label::textColourId, Colours::black);
    waveLabel.setText("Oscillator Waveform", dontSendNotification);
    waveLabel.attachToComponent(&waveChooserBox, false);
    
    waveChooserBox.addItem("Sine", 1);
    waveChooserBox.addItem("Square", 2);
    waveChooserBox.addItem("Saw", 3);
    waveChooserBox.addItem("Triagle", 4);
    waveChooserBox.setSelectedId(1);
    waveChooserBox.addListener(this);
    addAndMakeVisible(waveChooserBox);
    
    melodyLabel.setColour(Label::textColourId, Colours::black);
    melodyLabel.setText("Colour Selector", dontSendNotification);
    melodyLabel.attachToComponent(&melodyColourChooserBox, false);
    
    melodyColourChooserBox.addItem("Red Melody", 1);
    melodyColourChooserBox.addItem("Green Melody", 2);
    melodyColourChooserBox.addItem("Blue Melody", 3);
    melodyColourChooserBox.setSelectedId(1);
    melodyColourChooserBox.addListener(this);
    addAndMakeVisible(melodyColourChooserBox);
    
    sortingLabel.setColour(Label::textColourId, Colours::black);
    sortingLabel.setText("Sorting Method", dontSendNotification);
    sortingLabel.attachToComponent(&sortingChooserBox, false);
    
    sortingChooserBox.addItem("Line by line", 1);
    sortingChooserBox.addItem("By key", 2);
    sortingChooserBox.setSelectedId(1);
    sortingChooserBox.addListener(this);
    addAndMakeVisible(sortingChooserBox);
    
    playButton.setButtonText ("Play / Pause");
    playButton.setColour(TextButton::buttonColourId, Colours::seagreen);
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    stopButton.setButtonText ("Stop");
    stopButton.setColour(TextButton::buttonColourId, Colours::darkred);
    stopButton.addListener(this);
    addAndMakeVisible(&stopButton);
    
    tempoLabel.setColour(Label::textColourId, Colours::black);
    tempoLabel.setText("Tempo", dontSendNotification);
    tempoLabel.attachToComponent(&tempoSlider, false);
    
    tempoSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    tempoSlider.setColour(Slider::thumbColourId, Colours::seashell);
    tempoSlider.setRange(0.01, 0.99, 0.01);
    tempoSlider.setValue(0.5);
    tempoSlider.addListener(this);
    addAndMakeVisible(&tempoSlider);
    
    progressBar.setSliderStyle(juce::Slider::LinearBarVertical);
    progressBar.setRange(0.0, 1.0, 0.01);
    progressBar.setValue(1.0);
    addAndMakeVisible(&progressBar);
    
    volumeLabel.setColour(Label::textColourId, Colours::black);
    volumeLabel.setText("Volume", dontSendNotification);
    volumeLabel.attachToComponent(&volumeSlider, false);
    
    volumeSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    volumeSlider.setColour(Slider::thumbColourId, Colours::seashell);
    volumeSlider.setRange(0.01, 0.99, 0.01);
    volumeSlider.setValue(0.5);
    volumeSlider.addListener(this);
    addAndMakeVisible(&volumeSlider);
}

ImagePlayerGui::~ImagePlayerGui()
{
    
}

// ======================================================================================
// ======================================================================================

//Component function
void ImagePlayerGui::resized()
{
    fileChooser->setBounds          (                   10,              10, getWidth() * 0.5 - 10,               25);
    imgPreview.setBounds            (                   50, getHeight()*0.2, getWidth() * 0.5 - 55, getHeight()*0.50);
    progressBar.setBounds           (                   10, getHeight()*0.2,                    30, getHeight()*0.50);
    waveChooserBox.setBounds        ( getWidth() * 0.5 + 5, getHeight()*0.1, getWidth() * 0.5 - 15,               25);
    melodyColourChooserBox.setBounds( getWidth() * 0.5 + 5, getHeight()*0.2,      getWidth() * 0.2,               25);
    sortingChooserBox.setBounds     (    getWidth() * 0.79, getHeight()*0.2,      getWidth() * 0.2,               25);
    playButton.setBounds            ( getWidth() * 0.5 + 5, getHeight()*0.3,      getWidth() * 0.2, getHeight()*0.25);
    stopButton.setBounds            (    getWidth() * 0.79, getHeight()*0.3,      getWidth() * 0.2, getHeight()*0.25);
    tempoSlider.setBounds           (getWidth() * 0.5  + 5, getHeight()*0.7, getWidth() * 0.5 - 15,               25);
    volumeSlider.setBounds          (getWidth() * 0.5  + 5, getHeight()*0.8, getWidth() * 0.5 - 15,               25);
}

// ======================================================================================
// ======================================================================================

//ButtonListener function
void ImagePlayerGui::buttonClicked(Button *button)
{
    if (button == &playButton)
    {
        
        imagePlayer.setPlaying(!imagePlayer.isPlaying());
        
        if (imagePlayer.isPlaying())
        {
            startTimer(250);
        }
        else if (!imagePlayer.isPlaying())
        {
            stopTimer();
        }
    }
    else if (button == &stopButton)
    {
        imagePlayer.stop();
        progressBar.setValue(1.0);
    }
}

// ======================================================================================
// ======================================================================================

//FilenameComponentListener function
void ImagePlayerGui::filenameComponentChanged(FilenameComponent *fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File imageFile (fileChooser->getCurrentFile().getFullPathName());
        Image img = ImageFileFormat::loadFrom(imageFile);
        
        if(imageFile.existsAsFile())
        {
            imgPreview.setImages(false, true, true, img, 1.0, Colour(0x00ffffff), img, 1.0, Colour(0x00ffffff), img, 1.0, Colour(0x00ffffff));
            
            imagePlayer.processImage(img);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

// ======================================================================================
// ======================================================================================

void ImagePlayerGui::comboBoxChanged(ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &waveChooserBox)
    {
        if (waveChooserBox.getSelectedId() == 1)
        {
            audio.setWaveform (Audio::Sine);
        }
        else if (waveChooserBox.getSelectedId() == 2)
        {
            audio.setWaveform (Audio::Square);
        }
        else if (waveChooserBox.getSelectedId() == 3)
        {
            audio.setWaveform (Audio::Saw);
        }
        else if (waveChooserBox.getSelectedId() == 4)
        {
            audio.setWaveform (Audio::Triangle);
        }
    }
    else if (comboBoxThatHasChanged == &melodyColourChooserBox)
    {
        if (melodyColourChooserBox.getSelectedId() == 1)
        {
            imagePlayer.setMelodyColour(ImagePlayer::Red);
        }
        else if (melodyColourChooserBox.getSelectedId() == 2)
        {
            imagePlayer.setMelodyColour(ImagePlayer::Green);
        }
        else if (melodyColourChooserBox.getSelectedId() == 3)
        {
            imagePlayer.setMelodyColour(ImagePlayer::Blue);
        }
    }
    else if (comboBoxThatHasChanged == &sortingChooserBox)
    {
        if (sortingChooserBox.getSelectedId() == 1)
        {
            imagePlayer.setSortingMethod(ImagePlayer::ByLine);
        }
        else if (sortingChooserBox.getSelectedId() == 2)
        {
            imagePlayer.setSortingMethod(ImagePlayer::ByKey);
        }
    }
}

// ======================================================================================
// ======================================================================================

//SliderListener function
void ImagePlayerGui::sliderValueChanged(Slider *slider)
{
    if (slider == &tempoSlider)
    {
        imagePlayer.setTempo( 1 - tempoSlider.getValue());
    }
    else if (slider == &volumeSlider)
    {
        imagePlayer.setVolume(volumeSlider.getValue());
    }
}

// ======================================================================================
// ======================================================================================

//TimerCallback
void ImagePlayerGui::timerCallback()
{
    if (imagePlayer.isPlaying())
    {
        progressBar.setValue( 1 - imagePlayer.getPosition() );
    }
}

