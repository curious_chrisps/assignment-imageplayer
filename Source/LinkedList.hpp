//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Christoph Schick on 09/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>
#include <iostream>
using namespace std;
/**
 Linked list template.
 Supports conventional data types and provides a number of functions
 to execute standard operations on a singly-linked list.
 */

template <class Type>
class LinkedList
{
public:
    
    /** Creates a new, empty Linked List. */
    LinkedList ()
    {
        head = nullptr;
    }
    
    /** Deletes each node in the Linked List. */
    ~LinkedList ()
    {
        if (!isEmpty())
        {
            Node* temp1 = head->next;
            
            while(temp1!=NULL)
            {
                head->next = temp1->next;
                temp1 = NULL;
                delete temp1;
                if (head -> next != NULL)
                    temp1 = head->next;
            }
        }
    }
    
    /** Adds a new item at the end of the Linked List. */
    void addNode (Type itemValue)
    {
        Node* newNode = new Node();
        newNode->value = itemValue;
        newNode->next = nullptr;
        
        /* check if there are nodes in the list. If there aren't, set head to point to the new node. */
        if ( head == nullptr )
        {
            head = newNode;
        }
        else                                    /* Otherwise, make a temporary pointer that points to the next node. */
        {
            Node* temp = head;
            
            while ( temp->next != nullptr )     /* Execute the while loop until the temp pointer points to the end (nullptr). */
            {
                temp = temp->next;              /* Assign temporary pointer to be the next 'next' pointer. */
            }
            temp->next = newNode;               /* If the temporary pointer points to nullptr, make it point to the new node. */
        }
    }
    
    /** Returns the current number of nodes. */
    int getSize () const
    {
        if (head == nullptr)
        {
            return 0;
        }
        else
        {
            int counter = 0;
            Node* temp = head;
            
            while (temp != nullptr)
            {
                temp = temp->next;
                counter++;
            }
            return counter;
        }
    }
    
    /** Prints all elements of the list in order. */
    void printList()
    {
        if (getSize() == 0)
        {
            cout << "\n\n NOTHING TO PRINT HERE!\n\n";
            return;
        }

        Node* temp = head;
        cout << "\n\n List is: ";
        
        while (temp != nullptr)
        {
            cout << temp->value << "\n";
            temp = temp->next;
        }
    }
    
    /** Removes the node at a given index. */
    void removeNodeAtIndex(int index)
    {
        // make sure index is within list
        if ( index > getSize() || index < 0)
        {
            std::cout << "\n\n WARNING: Given index out of bounds, you daredevil!\n\n";
            return;
        }
        else if (index == 0)
        {
            Node* temp = head;
            head = head->next;
            delete temp;
        }
        else
        {
            // make temporary Node pointer
            // iterate to given index
            int counter = 0;
            Node* countPtr = head;
            Node* prevPtr = nullptr;
            Node* temp = nullptr;
            
            while ( counter <= index)
            {
                if(counter == index - 1)             // make prevPtr point to the node before the index
                {
                    prevPtr = countPtr;
                    temp = countPtr->next;
                }
                
                if(countPtr != nullptr)
                {
                    countPtr = countPtr->next;           // ITERATION let temp point to Node at index
                }
                
                if (counter == index)                // make the previous node->next point to the next-next element
                    prevPtr->next = countPtr;
                
                counter++;
            }
            delete temp;
        }
    }
    
    /** Returns true if the list is empty.*/
    bool isEmpty() const
    {
        return head == nullptr;
    }
    
    /**
      Clears the list by deleting all the nodes and assigning the head to be a nullptr.
      This way the list can be filled again using the addNode (Type itemValue) function
     */
    void clearList()
    {
        if (!isEmpty())
        {
            Node* temp1 = head->next;
            int iterationCounter = 0;
            
            std::cout << "\n\n DELETING NODES:\n";
            while(temp1 != NULL) // as I am considering tail->next = NULL
            {
                head->next = temp1->next;
                std::cout << "iter: "<< iterationCounter++ << "\tvalue: " << temp1->value << "\n";
                temp1 = NULL;
                delete temp1;
                if (head -> next != NULL)
                    temp1 = head->next;
            }
            head = nullptr;
            std::cout << "\n\n NODES DELETED!\n";
        }
    }
    
    /** Returns the item at a given index. */
    Type getValueAtIndex (int index) const
    {
        if ( index > getSize())
        {
            std::cout << "\n\n Argument is beyond the end of list! \nWhy on earth would you do this???\n\n";
            return 0;
        }
        else if (index < 0)
        {
            std::cout << "\n\n Argument is befor the start of list! \nWhy on earth would you do this???\n\n";
            return 0;
        }
        
        int elementCounter = 0;
        Node* temp = head;
        
        while ( elementCounter < index && temp->next != nullptr )
        {
            temp = temp->next;
            elementCounter++;
        }
        
        return temp->value;
    }
    
    
    /** Reverses the entire list. */
    void reverseList() 
    {
        if (getSize() < 2)
        {
            cout << "\n Not enough elements in List.";
            return;
        }
        else
        {
            cout << "\n\nREVERSING...";
            Node* tempPrev = nullptr;
            Node* tempCurrent = head;
            Node* tempNext = nullptr;
            
            while (tempCurrent != nullptr)
            {
                tempNext = tempCurrent->next;
                tempCurrent->next = tempPrev;
                tempPrev = tempCurrent;
                tempCurrent = tempNext;
            }
            head = tempPrev;
        }
    }
    
    
    /**
     Overloads the == operator in order to compare two linked lists.
     Returns true if length and values at same indeces are identical.
     */
    bool operator == (const LinkedList& otherList)
    {
        if (getSize() != otherList.getSize())
        {
            cout << "\n\n WARNING: Lists have differnt number of elements!\n";
            return false;
        }
        else
        {
            bool res = false;
            
            for (int i = 0; i < getSize(); i++)
            {
                if (getValueAtIndex(i) == otherList.getValueAtIndex(i))
                {
                    res = true;
                }
                else
                {
                    cout << "\n\n WARNING: Elements at index " << i << " not the same!\n";
                    return false;
                }
            }
            return res;
        }
        return false;
    }
    
    /**
     Overloads the [] operator so a list item can be retrieved using the conventional array[index] syntax.
     */
    Type& operator [](int index)
    {
        if ( index > getSize() || index < 0)
        {
            std::cout << "\n\n Argument is beyond the end of list! \n... Or befor the start... \nWho knows?\n\n";
            return head->value;
        }
       
            int elementCounter = 0;
            Node* temp = head;
            
            while ( elementCounter < index && temp->next != nullptr )
            {
                temp = temp->next;
                elementCounter++;
            }
            
            return temp->value;
    }
    
    
private:
    
    struct Node
    {
        Type value;
        Node* next;
    };
    
    Node* head;
    
};

#endif /* LinkedList_hpp */
