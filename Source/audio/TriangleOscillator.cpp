//
//  TriangleOscillator.cpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 29/11/2017.
//

#include "TriangleOscillator.hpp"
#include <cmath>

TriangleOscillator::TriangleOscillator()
{
    for (int n=0; n < NUM_TRI_HARMS; n++)
    {
        sinHarms[n].reset();
    }
}


float TriangleOscillator::renderWaveShape(const float currentPhase)
{
    int oddHarmCount = 1;
    
    for (int count=0; count < NUM_TRI_HARMS; count++)
    {
        sinHarms[count].setFrequency((oddHarmCount) * frequency);
        oddHarmCount = oddHarmCount + 2;
        sinHarms[count].setAmplitude(1.0);
    }
    
    float value = 0.0;
    
    for (int count=0; count < NUM_TRI_HARMS; count++)
    {
        if (count % 2 != 0)
        {
            value += (sinHarms[count].nextSample() * -1 ) * (1.0 / (count+1));
        }
        else
        {
        value += sinHarms[count].nextSample() * (1.0 / (count+1)); // the +1 is because it's the 1st harmonic, but 0th array item
        }
    }
    return value;
}
