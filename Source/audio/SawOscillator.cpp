//
//  SawOscillator.cpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 27/11/2017.
//

#include "SawOscillator.hpp"
#include <cmath>

SawOscillator::SawOscillator()
{
    for (int n=0; n < NUM_SAW_HARMS; n++)
    {
        sinHarms[n].reset();
    }
}


float SawOscillator::renderWaveShape(const float currentPhase)
{
    for (int count=0; count < NUM_SAW_HARMS; count++)
    {
        sinHarms[count].setFrequency((count+1) * frequency);
        sinHarms[count].setAmplitude(1.0);
    }
    
    float value = 0.0;
    
    for (int count=0; count < NUM_SAW_HARMS; count++)
    {
        value += sinHarms[count].nextSample() * (1.0 / (count+1)); // the +1 is because it's the 1st harmonic, but 0th array item
    }
    return value;
}
