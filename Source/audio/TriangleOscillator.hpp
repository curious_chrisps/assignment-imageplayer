//
//  TriangleOscillator.hpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 29/11/2017.
//

#ifndef TriangleOscillator_hpp
#define TriangleOscillator_hpp

#include "Oscillator.hpp"
#include "SinOscillator.h"

#define NUM_TRI_HARMS 25

/**
 Class for a saw-wave oscillator.
 */
class TriangleOscillator : public Oscillator
{
public:
    //==============================================================================
    TriangleOscillator();
    
    /**
     Function that provides the execution of the waveshape.
     */
    float renderWaveShape (const float currentPhase) override;
    
private:
    SinOscillator sinHarms[NUM_TRI_HARMS];
};

#endif /* TriangleOscillator_hpp */
