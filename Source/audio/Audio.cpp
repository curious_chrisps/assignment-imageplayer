/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    setWaveform (Sine);
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.setMidiInputEnabled("ImagePlayer Output", true);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

// ======================================================================================
// ======================================================================================

void Audio::setWaveform (Waveform newWaveform)
{
    if (newWaveform == Sine)
        oscPtr = &sinOsc;
    else if (newWaveform == Square)
        oscPtr = &squOsc;
    else if (newWaveform == Saw)
        oscPtr = &sawOsc;
    else if (newWaveform == Triangle)
        oscPtr = &triOsc;
}

// ======================================================================================
// ======================================================================================

void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    if (message.isNoteOn())
    {
        oscPtr.get()->setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
        oscPtr.get()->setAmplitude(message.getFloatVelocity());
        isOnNew = 1.0;
    }else if (message.isNoteOff())
    {
        isOnNew = 0.0;
    }
}

// ======================================================================================
// ======================================================================================

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    audioVolume = imagePlayer.getVolume();
    
    double oscVal;
    
    
    while(numSamples--)
    {
        if (imagePlayer.isPlaying())
        {
            isOnOld = (isOnNew * 0.1) + (isOnOld * 0.9);
        }
        else
        {
            isOnOld = isOnNew;
        }
        

        oscVal = oscPtr.get()->nextSample();
        
        *outL = oscVal  * isOnOld * audioVolume;
        *outR = oscVal  * isOnOld * audioVolume;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

// ======================================================================================
// ======================================================================================

void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sinOsc.setSampleRate (device->getCurrentSampleRate());
    squOsc.setSampleRate (device->getCurrentSampleRate());
    sawOsc.setSampleRate (device->getCurrentSampleRate());
   
    isOnNew = 0.0;

}

// ======================================================================================
// ======================================================================================

void Audio::audioDeviceStopped()
{

}
