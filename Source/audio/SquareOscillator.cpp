//
//  SquareOscillator.cpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 23/11/2017.
//

#include "SquareOscillator.hpp"
#include <cmath>

float SquareOscillator::renderWaveShape(const float currentPhase)
{
    float inTheNowPhase = sin(currentPhase);
    
    if (inTheNowPhase > 0 )
    {
        return 1.f;
    }
    else if (inTheNowPhase < 0 )
    {
        return -1.f;
    }
    else
    {
        return 0.f;
    }
}
