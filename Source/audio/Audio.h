/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

/*
 ################## TO  DO  ###################
 - Enable polyphonic playback.
 - (Maybe) enable an outside device to be connected such that it can play together
     with the playback generated in this application.
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h"
#include "SquareOscillator.hpp"
#include "SawOscillator.hpp"
#include "TriangleOscillator.hpp"
#include "ImagePlayer.hpp"


/**
Class containing all audio processes.
*/

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
//========================================== P U B L I C ==================================================
public:
    /**
     Constructor
     */
    Audio();
    
    /**
     Destructor
     */
    ~Audio();
    
    /**
     Returns the audio device manager, don't keep a copy of it!
     */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    /**
     Returns a reference to the ImagePlayer instance used in this Audio class.
     */
    ImagePlayer& getImagePlayer() { return imagePlayer; }
    
    /**
     Selection of wave forms the user can choose to configure the oscillator.
     */
    enum Waveform
    {
        Sine,
        Square,
        Saw,
        Triangle
    };
    
    /**
     Updates the wave form that is used to generate the audio.
     @param newWaveform     One of the enum values the user can choose.
     */
    void setWaveform (Waveform newWaveform);
    
    /**
     Overridden function from MidiInputCallback.
     */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    
    /**
     Overridden functions from AudioIODeviceCallback.
     */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    void audioDeviceStopped() override;
    

    
    
//========================================== P R I V A T E ==================================================
private:
    AudioDeviceManager audioDeviceManager;
    Float32 isOnNew, isOnOld;
    float audioVolume;
    
    ImagePlayer imagePlayer;
    
    /** Atomic Oscillator pointer able to be pointed at any object of a class derived from Oscillator. */
    Atomic<Oscillator*> oscPtr;
    
    /** Oscillator objects. */
    SinOscillator sinOsc;
    SquareOscillator squOsc;
    SawOscillator sawOsc;
    TriangleOscillator triOsc;
    
    
};

#endif  // AUDIO_H_INCLUDED
