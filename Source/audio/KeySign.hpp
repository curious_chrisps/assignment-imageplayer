//
//  KeySign.hpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 29/01/2018.
//

#ifndef KeySign_hpp
#define KeySign_hpp

#include <stdio.h>
#include "../LinkedList.hpp"
/**
 Objects of this class are meant to store a collection of note numbers in a certain key signature.
 This class provides the option to test if a note number is permitted as part of the key the object is initialised with.
 @see LinkedList
 */
class KeySign   :   public LinkedList<int>
{
public:
    
    /** Collection of key signatures. Used to determine the base note of the Key. */
    enum Keys { Cmaj = 0, Dmaj, Emaj, Fmaj, Gmaj, Amaj, Bmaj, CshMaj, FshMaj};
    
    /**
     Initialises this list with a key.
     This function is called to fill the internal list with 'allowed notes' with the notes in a key.
     @param key     Key signature this instance is initialised with.
     */
    void initialiseWithKey( Keys key );
    
    /**
     Returns true if the given number is found in the initialised list of notes in the key.
     @param num     Number you want to check if it is in this key.
     @return        True, if number passed is found in notesInKey. False if not.
     */
    bool allowsNumber (int num);

private:
    int base;
    int numberOfMidiNotesInKey;
    LinkedList<int> notesInKey;
};

#endif /* KeySign_hpp */
