//
//  KeySign.cpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 29/01/2018.
//

#include "KeySign.hpp"

void KeySign::initialiseWithKey( Keys key )
{
    switch (key)
    {
        case Cmaj:
            base = 0;
            break;
            
        case Dmaj:
            base = 2;
            break;
            
        case Emaj:
            base = 4;
            break;
            
        case Fmaj:
            base = 5;
            break;
            
        case Gmaj:
            base = 7;
            break;
            
        case Amaj:
            base = 9;
            break;
            
        case Bmaj:
            base = 11;
            break;
            
        case CshMaj:
            base = 1;
            break;
            
        case FshMaj:
            base = 6;
            break;
            
        default:
            base = 0;
            break;
    }
    
    int intervalCount = 0;
    
    while (base < 128)
    {
        notesInKey.addNode(base);
        
        if ( intervalCount == 2 || intervalCount == 6 )
        {
            base += 1;
        }
        else
        {
            base +=2;
        }
        
        intervalCount++;
        if (intervalCount >= 7)
        {
            intervalCount = 0;
        }
    }
}

// ======================================================================================
// ======================================================================================

bool KeySign::allowsNumber(int num)
{
    for (int i = 0; i < notesInKey.getSize(); i++)
    {
        if (notesInKey[i] == num)
            return true;
    }
    return false;
}
