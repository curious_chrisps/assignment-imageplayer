//
//  SquareOscillator.hpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 23/11/2017.
//

#ifndef SquareOscillator_hpp
#define SquareOscillator_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"

/**
 Class for a squarewave oscillator.
 */
class SquareOscillator : public Oscillator
{
public:
    //==============================================================================
    /**
     Function that provides the execution of the waveshape.
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif /* SquareOscillator_hpp */
