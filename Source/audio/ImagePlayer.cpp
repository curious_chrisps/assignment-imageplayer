//
//  ImagePlayer.cpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 07/01/2018.
//

#include "ImagePlayer.hpp"

ImagePlayer::ImagePlayer()  :   Thread ("imagePlayerThread"),
                                currentListPosition(0),
                                imgInternalReference(imgInternalCopy)
{
    midiOutput = MidiOutput::createNewDevice ("ImagePlayer Output");
   
    keySignArray[0].initialiseWithKey(KeySign::Cmaj);
    keySignArray[1].initialiseWithKey(KeySign::Dmaj);
    keySignArray[2].initialiseWithKey(KeySign::Emaj);
    keySignArray[3].initialiseWithKey(KeySign::Fmaj);
    keySignArray[4].initialiseWithKey(KeySign::Gmaj);
    keySignArray[5].initialiseWithKey(KeySign::Amaj);
    keySignArray[6].initialiseWithKey(KeySign::Bmaj);
    keySignArray[7].initialiseWithKey(KeySign::CshMaj);
    keySignArray[8].initialiseWithKey(KeySign::FshMaj);
}

ImagePlayer::~ImagePlayer()
{
    delete midiOutput;
}


// ======================================================================================
// ======================================================================================

void ImagePlayer::processImage(Image& img)
{
    if (isPlaying())
    {
        return;
    }
    
    DBG("\n\nBefore clearList\nNumber of elements in  midiNoteNumbers list: " << midiNoteNumbers.getSize() << "\n");
    if (img.isValid()) {
        midiNoteNumbers.clearList();
    }
    DBG("\n\nAfter clearList\nNumber of elements in  midiNoteNumbers list: " << midiNoteNumbers.getSize() << "\n");
    
    imgInternalReference = img;
    currentListPosition = 0;
    
    
    int width = img.getWidth();
    int heigth = img.getHeight();
    Colour currentPixelcolour;
    int rowArray[width];
    int noteNumbers[heigth];
    int currentRowAverage;
    
    for (int hCounter = 0; hCounter < heigth; hCounter++)
    {
        for (int wCounter = 0; wCounter < width; wCounter++)
        {
            currentPixelcolour = img.getPixelAt(wCounter, hCounter);
            
            if (currentMelodyColour == Red) {
                rowArray[wCounter] = currentPixelcolour.getRed();
            }
            else if (currentMelodyColour == Green)
            {
                rowArray[wCounter] = currentPixelcolour.getGreen();
            }
            else if (currentMelodyColour == Blue)
            {
                rowArray[wCounter] = currentPixelcolour.getBlue();
            }
        }
        
        currentRowAverage = getAverageColour(rowArray, width);
        
        noteNumbers[hCounter] = currentRowAverage;
        
        ScopedLock sl(imageLock);
        midiNoteNumbers.addNode(currentRowAverage);
    }
    DBG("\n\nAfter processing\nNumber of elements in  midiNoteNumbers list: " << midiNoteNumbers.getSize() << "\n");
    
    if (currentSortingMethod == ByKey)
    {
        DBG("\nCalling the sortNotesByKeys function on midiNoteNumbers.\n");
        midiNoteNumbers = sortNotesByKeys(midiNoteNumbers);
    }

}


// ======================================================================================
// ======================================================================================

LinkedList<int>& ImagePlayer::sortNotesByKeys(LinkedList<int>& noteNumbers)
{
    DBG("\n ============ INSIDE SORTING FUNCTION =============\n");
    
    DBG("\n Clearing lists sortedNotes and keySign lists.\n");
    if (!sortedNotes.isEmpty())
    {
        sortedNotes.clearList();
    }
    
    for (int t = 0; t < NUM_KEY_SIGNATURES; t++)
    {
        if (!keySignArray[t].isEmpty())
        {
            keySignArray[t].clearList();
            DBG("\n Cleared keyList: " << t);
        }
    }
    
    DBG("\n Sorting notes by keys into different lists.\n");
    int keySwitch = 0;
    for (int i = 0; i < noteNumbers.getSize(); i++)
    {
        while (!keySignArray[keySwitch].allowsNumber(noteNumbers[i]))
        {
            DBG("\niteration: " << i << "   in while loop." );
            DBG("keySwitch: " << keySwitch );
            DBG("noteNumber[i]: " << noteNumbers[i] );
            keySwitch++;
            if (keySwitch >= NUM_KEY_SIGNATURES)
                keySwitch = 0;
        }
        keySignArray[keySwitch].addNode(noteNumbers[i]);
        
        keySwitch++;
        
        if (keySwitch >= NUM_KEY_SIGNATURES)
            keySwitch = 0;
        
    }
    DBG("\n Notes sorted now in different lists.\n");
    
    DBG("\n Joining the lists in one sortedNotes lists\n");
    for (int i = 0; i < NUM_KEY_SIGNATURES; i++)
    {
        if (!keySignArray[i].isEmpty())
        {
            for (int k = 0; k < keySignArray[i].getSize(); k++)
            {
                sortedNotes.addNode(keySignArray[i][k]);
            }
        }
    }
    DBG("\n Lists joined.\n");
    
    return sortedNotes;
}

// ======================================================================================
// ======================================================================================

int ImagePlayer::getAverageColour(int *rowArray, int size)
{
    int sum = 0;
    
    for (int i = 0; i < size; i++)
    {
        sum += rowArray[i];
    }
    
    return (sum/size) * 0.5;
}

// ======================================================================================
// ======================================================================================

void ImagePlayer::setMelodyColour(MelodyColour newMelodyColour)
{
    if (currentMelodyColour == newMelodyColour)
        return;
    
    currentMelodyColour = newMelodyColour;
    if (imgInternalReference.isValid())
    {
        stop();
        DBG("\nRe-Processing the image...\n");
        processImage(imgInternalReference);
        DBG("\nRe-Processing done!\n");
    }
}

// ======================================================================================
// ======================================================================================

void ImagePlayer::setSortingMethod(SortingMethod newSortingMethod)
{
    if (currentSortingMethod == newSortingMethod)
        return;
    
    currentSortingMethod = newSortingMethod;
    if (imgInternalReference.isValid())
    {
        stop();
        DBG("\nRe-Processing the image...\n");
        processImage(imgInternalReference);
        DBG("\nRe-Processing done!\n");
    }
}

// ======================================================================================
// ======================================================================================

void ImagePlayer::stop()
{
    currentListPosition = 0;
    playState = false;
    signalThreadShouldExit();
}

// ======================================================================================
// ======================================================================================

void ImagePlayer::setPlaying(const bool newState)
{
    playState = newState;
    
    if (playState == true)
    {
        if (imgInternalReference.isValid())
        {
            startThread();
        }
        else
        {
            playState = false;
        }
    }
    else if (playState == false)
    {
        signalThreadShouldExit();
    }
}

// ======================================================================================
// ======================================================================================

bool ImagePlayer::isPlaying () const
{
    return playState;
}

// ======================================================================================
// ======================================================================================

void ImagePlayer::run()
{
    while (!threadShouldExit() && currentListPosition.get() < midiNoteNumbers.getSize())
    {
        ScopedLock sl(imageLock);
        MidiMessage messageOn = MidiMessage::noteOn(1, midiNoteNumbers.getValueAtIndex(currentListPosition.get()), (uint8)100);
        MidiMessage messageOff (MidiMessage::noteOff (messageOn.getChannel(), messageOn.getNoteNumber()));
        midiOutput->sendMessageNow(messageOn);
        sleep(interval.get());
        
        midiOutput->sendMessageNow(messageOff);
        
        currentListPosition.set(currentListPosition.get() + 1);
    }
}

// ======================================================================================
// ======================================================================================

void ImagePlayer::setTempo(const float floValue)
{
    if (floValue > 1.0) // || floValue < 0.01)
    {
        DBG("\n\n WARNING: The value given to determine the interval is too large.  \n");
    }
    else if (floValue < 0.009)
    {
        DBG("\n\n WARNING: The value given to determine the interval is too small.  \n");
    }
    else
    {
        interval.set((int) (1000 * floValue));
    }
}

// ======================================================================================
// ======================================================================================

float ImagePlayer::getPosition()
{
    if (currentListPosition.get() > 0) {
        return (float)currentListPosition.get() / midiNoteNumbers.getSize();
    }
    else return 0.0;
}

// ======================================================================================
// ======================================================================================

void ImagePlayer::setVolume(const float newVolume)
{
    volume = newVolume;
}

// ======================================================================================
// ======================================================================================

float ImagePlayer::getVolume()
{
    return volume;
}



