//
//  SawOscillator.hpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 27/11/2017.
//

#ifndef SawOscillator_hpp
#define SawOscillator_hpp


#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"
#include "SinOscillator.h"

#define NUM_SAW_HARMS 50

/**
 Class for a saw-wave oscillator.
 */
class SawOscillator : public Oscillator
{
public:
    //==============================================================================
    SawOscillator();
    
    /**
     Function that provides the execution of the waveshape.
     */
    float renderWaveShape (const float currentPhase) override;
    
private:
    SinOscillator sinHarms[NUM_SAW_HARMS];
};

#endif /* SawOscillator_hpp */
