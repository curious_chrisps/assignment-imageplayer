//
//  ImagePlayer.hpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 07/01/2018.
//

/*
 ################## FUTURE DEVELOPMENT  ###################
 
 - Create option to play chords consisting of Red, Green and Blue notes.
 - If image has alpha/hue property, use it as amplitude for the notes.
 */

#ifndef ImagePlayer_hpp
#define ImagePlayer_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "../LinkedList.hpp"
#include "KeySign.hpp"

#define NUM_KEY_SIGNATURES 9

/**
 Class containing all image processing.
 It acts as a thread as well as a midi device.
 When running, the ImagePlayer sends out midimessages generated from the colour data of a loaded image.
 */
class ImagePlayer   :   private Thread
{
public:
    /**
     Constructor
     */
    ImagePlayer();
    
    /**
     Destructor
     */
    ~ImagePlayer();
    
    /**
     Processes the image.
     */
    void processImage(Image& img);
    
    /**
     Sorts the list midiNoteNumbers into different keys.
     @param noteNumbers     Linked list that hold the int numbers to be sorted.
     @return A sorted linked list.
     */
    LinkedList<int>& sortNotesByKeys(LinkedList<int>& noteNumbers);
    
    /**
     Stops the thread that sends the midimessages.
     */
    void stop();
    
    /**
     Is updated for internal record of whether the thread is running or not.
     */
    void setPlaying(const bool newState);
    
    /**
     Gets the current playback state of the ImagePlayer
     */
    bool isPlaying () const;
    
    /**
     Sets the interval in which the midimessages are sent.
     @param floValue    Expects a value between 0.01 and 0.99
     */
    void setTempo(const float floValue);
    
    /**
     Returns the midisequens' current playback position in a value from 0.0 to 1.0.
     */
    float getPosition();

    /**
     Updates the volume variable.
     */
    void setVolume(const float newVolume);
    
    /**
     Returns most recent volume value.
     */
    float getVolume();
    
    /**
     Enumeration that serves as variable to select which colour part is taken
     from the pixel and added to the midiNoteNumbers list.
     */
    enum MelodyColour
    {
        Red,
        Green,
        Blue
    };
    
    /**
     Enumeration that serves as variable to switch between sorting algorhythms.
     */
    enum SortingMethod
    {
        ByLine,
        ByKey
    };
    
    /**
     Updates the currentMelodyColour used to generate the midi note numbers.
     */
    void setMelodyColour(MelodyColour newMelodycolour);
    
    /**
     Updates the currentSortingMethod used to sort the midi note numbers.
     */
    void setSortingMethod(SortingMethod newSortingMethod);
    
//========================================= P R I V A T E ===================================================
private:
    /** A lock to enable the thred to send messages in the background. */
    CriticalSection imageLock;
    
    /** The thread function containing the loop that sends the midi messages. */
    void run() override;
    
    /**
     Used in processImage to determine the average value of the selected colour in one row of the image.
     @param     rowArray    Is the row of the image to be processed.
     @param     size        The size of rowArray.
     @return    The average of all numbers in the array.
     */
    int getAverageColour(int* rowArray, int size);
    
    
    /** List that holds all generated midi note numbers. */
    LinkedList<int> midiNoteNumbers;
    
    /** List for the sorted midi note numbers. */
    LinkedList<int> sortedNotes;
    
    /** Array of key signatured lists. These lists are used to sort all notes into keys.*/
    KeySign keySignArray[NUM_KEY_SIGNATURES];
    
    
    /** Atomic (thread safe) variables used in the ImagePlayer::run() function. */
    Atomic<int> interval;
    Atomic<int> currentListPosition;
    
    /** Variable that holds the information whether the thread is running or not. */
    bool playState = false;
    
    /** Variable that holds the current volume. */
    float volume;
    
    /** Enum variable that holds the currently selected colour mode. */
    MelodyColour currentMelodyColour;
    
    /** Enum variable that holds the currently selected sorting method. */
    SortingMethod currentSortingMethod;
    
    /** Used to create a new midi device in this class' constructor. */
    MidiOutput* midiOutput;
    
    /**
     Internal reference to the loaded image.
     Used to acces the image and pass it to the processImgae function after certain parameters have changed.
     */
    Image& imgInternalReference;
    Image imgInternalCopy;
   
};

#endif /* ImagePlayer_hpp */

