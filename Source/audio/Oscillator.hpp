//
//  Oscillator.hpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 24/11/2017.
//

#ifndef Oscillator_hpp
#define Oscillator_hpp

#include <stdio.h>


#include "../JuceLibraryCode/JuceHeader.h"

/**
 Class for an oscillator.
 This is a pure virtual base class containing the basic functions of an oscillator EXCEPT the waveshape.
 For the sake of not repeating too much code, specifically "shaped" oscillators can be created by declaring a class,
 inheriting from this class and overriding its renderWaveShape function.
 
 @see SinOscillator, SquareOscillator, SawOscillator, TriangleOscillator
 */

class Oscillator
{
public:
    
    friend class SinOscillator;
    friend class SquareOscillator;
    friend class SawOscillator;
    friend class TriangleOscillator;
    
    //==============================================================================
    /**
     Oscillator constructor
     */
    Oscillator();
    
    /**
     Oscillator destructor
     */
    virtual ~Oscillator();
    
    /**
     Sets the frequency of the oscillator.
     @param freq            The precice frequency value in which the oscillator will generate the sound at.
     */
    void setFrequency (float freq);
    
    /**
     Sets frequency using a midi note number.
     Calculates the frequency of the oscillator from the MIDI note number.
     
     @param noteNum         Is the MIDI note number.
     */
    void setNote (int noteNum);
    
    /**
     Sets the amplitude of the oscillator.
     */
    void setAmplitude (float amp);
    
    /**
     Resets the oscillator.
     */
    void reset();
    
    /**
     Sets the sample rate.
     */
    void setSampleRate (float sr);
    
    /**
     Takes all given parameters, calculates the next sample and returns it.
     
     @return    The next sample to be placed into the output buffer.
     */
    float nextSample();
    
    /**
     Pure virtual function to be defined by "descendents" to produce particular waveshape.
     
     @return    The next part of the waveshape to be used to calculate the sample.
     */
    virtual float renderWaveShape (const float currentPhase) = 0;
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
};

#endif /* Oscillator_hpp */
